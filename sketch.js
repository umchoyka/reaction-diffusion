//Gray-Scott Reaction-Diffusion algorithm implementation in JavaScript
// using p5js framework and Typed Arrays

// Define canvas height and width parameters
var cWidth = 500;
var cHeight = 400;

// Variables for storing and manipulating canvas pixel data without using the
// built-in P5js pixels array.
var ctx;
var imageData;
var gBuf;
var gBuf8;
var gData;

// Display framerate in a separate html <P>
var pFR;

// Data for the algorithm implemented using typed arrays
var currBuff;
var currView;
var nextBuff;
var nextView;

// Algorithm constants
const DA = 1;
const DB = 0.5;

// F default 0.55, K default 0.62
// Cool combos:
//  F = 0.049, K = 0.06
//  F = 0.045, K = 0.063
//  F = 0.017, K = 0.057
//  F = 0.017, K = 0.045
const F = 0.016;
const K = 0.044;

function setup() {
  createCanvas(cWidth, cHeight, P2D);
  pixelDensity(1);

  // Refer to the https://hacks.mozilla.org/2011/12/faster-canvas-pixel-manipulation-with-typed-arrays/
  // article for more in depth info on using typed array image buffers instead
  // of the P5js built-in pixels array.
  let canvas = document.getElementById('defaultCanvas0');
  ctx = canvas.getContext('2d');
  imageData = ctx.getImageData(0, 0, cWidth, cHeight);

  // ArrayBuffer reserves a block of memory for the array. The intent is to use
  // 32-bit unsigned integers to assign pixel color values
  gBuf = new ArrayBuffer(imageData.data.length);
  // However, the actual image data function expects an array of 8-bit integers.
  // Luckily we can use two different views to manipulate and read the data.
  gBuf8 = new Uint8ClampedArray(gBuf);
  gData = new Uint32Array(gBuf);

  // Similarly, our algorithm uses fixed arrays of Float values. The memory
  // calculation is to use a pair of floating point numbers ("chemical a" and
  // "chemical b") for each pixel of the canvas. Floating point numbers are
  // four bytes in length, so the resulting buffer required is Width * Height *
  // 4 (bytes per float) * 2 (variables "a" and "b").
  currBuff = new ArrayBuffer(cWidth * cHeight * 4 * 2);
  nextBuff = new ArrayBuffer(cWidth * cHeight * 4 * 2);
  currView = new Float32Array(currBuff);
  nextView = new Float32Array(nextBuff);

  // Seed the arrays, each pair of floating point numbers is arranged [a, b] and
  // paired adjacent to each other in the array.
  for (let i = 0; i < currView.length; i += 2) {
    currView[i] = 1.0;
    currView[i + 1] = 0.0;
    nextView[i] = 1.0;
    nextView[i + 1] = 0.0;
  }


  // Randomly assign some amount of "chemical b" within the "current" array, as
  // per the original code. Here I have added some number of initial rectangular
  // blotches of chemical b.
  for (var n = 0; n < random(6, 12); n++) {
    let iLoc = floor(random(16, cWidth - 56));
    let jLoc = floor(random(16, cHeight - 56));
    for (let i = iLoc; i < iLoc + random(6, 50); i++) {
      for (let j = jLoc; j < jLoc + random(6, 50); j++) {
        random(1) > 0.4 ? currView[2 * (i + j * cWidth) + 1] = 1.0 : '';
      }
    }
  }

  pFR = createP();
}

function draw() {
  // As per the original algorithm, ignore the edge cases.
  for (let i = 1; i < cWidth - 1; i++) {
    for (let j = 1; j < cHeight - 1; j++) {
      // x is our index into the data arrays. The calculation is identical to
      // the original algorithm's pixel array index. The 2* multiplier is
      // because each pair of our "chemical" data is stored together, thus
      // we need to have our index increment twice for each loop.
      let x = 2 * (i + j * cWidth);
      let a = currView[x];
      let b = currView[x + 1];

      nextView[x] = a +
        (DA * laPlace(0, x)) -
        (a * b * b) +
        (F * (1 - a));

      nextView[x + 1] = b +
        (DB * laPlace(1, x)) +
        (a * b * b) -
        ((K + F) * b);

      // Rewrite of the original algorithm's if statement using ternary
      // operators. For more on Ternary operators see:
      // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator
      // I'm not sure if this provides a more efficient resolution vs using if
      // but I added these for practice.
      nextView[x] = nextView[x] > 1 ? 1 : nextView[x] < 0 ? 0 : nextView[x];
      nextView[x + 1] = nextView[x + 1] > 1 ? 1 : nextView[x + 1] < 0 ? 0 : nextView[x + 1];
    }
  }

  for (let i = 0; i < gData.length; i++) {
    // x is used to index our data for passing updating the image
    let x = 2 * i;

    // Refer to the Mozilla Hacks article for more in-depth information.
    // Essentially, we are assigning the RGBA values to the image buffer (but
    // in reverse order due to the default Endianness of the buffer). This is
    // analagous to the assignment of the RGBA values to the pixel array in P5.
    gData[i] =
      (255 << 24) |
      (floor(nextView[x + 1] * 255) << 16) |
      (0 << 8) |
      (floor(nextView[x] * 255));
  }
  // This code replaces the updatePixels function of P5.
  imageData.data.set(gBuf8);
  ctx.putImageData(imageData, 0, 0);
  swap();
  pFR.html(frameRate());
}

function laPlace(aorb, x) {
  let sum = 0;
  //Edge cases
  // let mx = x % (2 * cWidth);
  // let iMin = (mx == 0) ? 0 : -1;
  // let iMax = (mx == 2 * (cWidth - 1)) ? 0 : 1;
  // let jMin = ((x - 2 * cWidth) <= 0) ? 0 : -1;
  // let jMax = ((x + 2 * cWidth) >= 2 * cWidth * cHeight) ? 0 : 1;
  //
  // let cW = -iMin - jMin + jMax + iMax;
  // cW = cW == 2 ? -0.3 : cW == 1 ? -0.7 : -1.0;

  //To introduce edge cases, comment out the let statements below and uncomment
  // the ones above. Also, change the bounds of the first draw() loop to include
  // the first and last indicies of the grid.

  // NOTE: The edge case calculation includes a modulo math operator which is
  //       a very costly operation. You can use a bitwise or operator instead
  //       but you must restrict the canvas width to a power of 2.

  let iMin = -1;
  let iMax = 1;
  let jMin = -1;
  let jMax = 1;
  let cW = -1;

  for (var i = iMin; i <= iMax; i++) {
    for (var j = jMin; j <= jMax; j++) {
      let factor = (i != 0 ? j != 0 ? 0.05 : 0.2 : j != 0 ? 0.2 : cW);
      sum += currView[x + (2 * (i + (j * cWidth))) + aorb] * factor;
    }
  }
  // sum = cW == -0.3 ? sum / 0.3 : cW == -0.7 ? sum / 0.7 : sum;
  return sum;
}

function swap() {
  let temp = currView;
  currView = nextView;
  nextView = temp;
}