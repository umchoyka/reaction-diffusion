## Reaction-Diffusion algorithm
A JavaScript implementation of the Gray-Scott Reaction Diffusion algorithm. Code is based on a video tutorial by Daniel Shiffman (www.thecodingtrain.com).

### Modifications
The source code from the tutorial uses static,  JavaScript untyped arrays and class expressions to store the data for the algorithm. It also makes use of built-in functions from the p5js library for image processing. The resulting code works but runs quite slowly, even on modern hardware.

This version of the code removes all of those data structures and replaces them with JavaScript untyped arrays. The code suffers in readability but performance is enhanced. The end result is the algorithm runs at more than four times the original framerate.

Typed arrays are a special array in JavaScript that are a single, sequential block of reserved memory. JavaScript untyped arrays are typically stored as a single continuous block of memory, but come with additional processing overhead to allow versatility. Additionally, when untyped arrays are modified (by adding and / or removing elements) the array is transformed into a linked list and becomes extremely inefficient.

### JavaScript Typed Arrays References
I used the following websites as reference materials to guide me in implementing the typed arrays:

#### Mozilla JavaScript web docs on Typed Arrays
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Typed_arrays
https://developer.mozilla.org/en-US/docs/Web/API/ImageData

#### Mozilla Hacks article on Canvas
Credit to Paul Rouget:
https://hacks.mozilla.org/2011/12/faster-canvas-pixel-manipulation-with-typed-arrays/

### Implementation of Typed Arrays
For this project, implementing typed arrays is thankfully straightforward. The original code base used *static* arrays (i.e. they did not change length) to store *only* floating point numbers. This type of data structure lends itself well to converting to typed arrays as they are most useful when you can assign a single block of memory for processing arrays of one type of variable.

The key differences to using typed arrays are that you **must** calculate the memory required by the array (based on what type of variable you intend to use), and that they can only store one type of variable in any given array\*. The setup of the array is also done in two parts: first creating a "buffer" in memory which is an immutable array object that reserves the memory block and second using "views" to manipulate the data. The Mozilla MDN web docs linked above have a great description of the workings of these objects.

Canvas image data can be manipulated in the same way as the above. The key point is realizing that the ImageData is essentially a typed array (of sequential unsigned, 8-bit integers) and creating the associated buffer and view to manipulate the data directly. I tried to look through the P5js source code to identify the underlying structure used in the loadPixels() and updatePixels() functions but I cannot make a confident statement on exactly what is being used. Someone with better knowledge of the P5js project will have to comment on its implementation.

One shorthand used in the view of the imageData is using a 32-bit unsigned integer data type, making the view behave very similarly to the P5js pixels[] array.

\* Technically speaking, the buffer does not understand its contents. Therefore, you can use different views on the data to manipulate its contents *as though* it were different data types. The Mozilla MDN articles show examples of this in action. For the purposes of this project, this feature is used to assign 32-bit pixel data to the image buffer, and then pass it back to the canvas as the 8-bit data it expects.
